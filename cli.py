#!/usr/bin/env python3
import click


def load_rolls(filename):
    with open(filename, 'r') as f:
        content = f.read()
    rolls = []
    for section in content.split('\n\n'):
        roll = {}
        lines = section.split('\n')
        for line in lines:
            if not line.strip():
                continue
            k, v = line.split(':', 1)
            k, v = k.strip(), v.strip()
            if k in ('roll', 'length'):
                roll[k] = int(v)
            else:
                if k not in roll:
                    roll[k] = v
                else:
                    if type(roll[k]) is list:
                        roll[k].append(v)
                    else:
                        roll[k] = [roll[k], v]
        rolls.append(roll)
    return rolls


def find_roll(rolls, roll_number):
    for roll in rolls:
        if roll['roll'] == roll_number:
            return roll
    return None


def count_cameras(rolls):
    cameras = {}
    for roll in rolls:
        camera = roll['camera']
        cameras[camera] = cameras.get(camera, 0) + 1
    return cameras


def count_lenses(rolls):
    lenses = {}
    for roll in rolls:
        if 'lens' not in roll:
            continue
        roll_lenses: Dict[str, int] = []
        if type(roll['lens']) is list:
            roll_lenses = roll['lens']
        else:
            roll_lenses = [roll['lens']]
        for lens in roll_lenses:
            lenses[lens] = lenses.get(lens, 0) + 1
    return lenses


def count_films(rolls):
    films = {}
    for roll in rolls:
        film = roll['film']
        films[film] = films.get(film, 0) + 1
    return films


def select_from_list_or_new(item_name, items):
    while True:
        answer = input(f'{item_name.title()}: ')
        if not answer.isdigit():
            break
        elif int(answer) < len(items):
            answer = items[int(answer)]
            break
        else:
            print('No such {item_name}.')
        continue
    return answer


def print_roll(roll):
    for k, vs in roll.items():
        if type(vs) is not list:
            vs = [vs]
        for v in vs:
            click.echo('%11s:\t%s' % (k, v))


@click.group()
def cli() -> None:
    pass


@cli.command(name='rolls')
@click.pass_obj
def cli_print_rolls(rolls):
    """ Print all rolls. """
    for roll in rolls:
        print_roll(roll)
        click.echo()
    click.secho(f'{len(rolls)} rolls', fg='yellow')


@cli.command(name='roll')
@click.argument('roll_number', type=int)
@click.pass_obj
def cli_print_roll(rolls, roll_number):
    """ Print roll. """
    roll = find_roll(rolls, roll_number)
    if not roll:
        click.secho('No such roll.', fg='red')
    else:
        print_roll(roll)


@cli.group(name='count')
def cli_count() -> None:
    """ Count usage. """
    pass


@cli_count.command(name='lenses')
@click.pass_obj
def cli_count_lenses(rolls):
    """ Count lenses. """
    lenses = count_lenses(rolls)
    counts = [(count, lens) for lens, count in lenses.items()]
    for count, lens in reversed(sorted(counts)):
        click.echo(f'{count}\t{lens}')


@cli_count.command(name='cameras')
@click.pass_obj
def cli_count_cameras(rolls):
    """ Count cameras. """
    cameras = count_cameras(rolls)
    counts = [(count, camera) for camera, count in cameras.items()]
    for count, camera in reversed(sorted(counts)):
        click.echo(f'{count}\t{camera}')


@cli_count.command(name='films')
@click.pass_obj
def cli_count_films(rolls):
    """ Count films. """
    films = count_films(rolls)
    counts = [(count, film) for film, count in films.items()]
    for count, film in reversed(sorted(counts)):
        click.echo(f'{count}\t{film}')


@cli.command(name='stat')
@click.pass_obj
def cli_stat(rolls):
    """ Stats. """
    years = {}
    for roll in rolls:
        year = int(roll['exposed'][:4])
        years[year] = years.get(year, 0) + 1
    for year, count in sorted(years.items()):
        print(f'{year}: ' + ('*' * count) + f' ({count})')
    print(f'total: {sum(years.values())}')


@cli.command(name='add')
@click.pass_obj
def cli_add(rolls):
    """ Add roll. """
    new_roll = {}
    next_roll_number = max(r['roll'] for r in rolls) + 1
    roll_number = input(f'Roll number [{next_roll_number}]: ')
    if not roll_number:
        roll_number = next_roll_number
    else:
        roll_number = int(roll_number)
    cameras = list(sorted(count_cameras(rolls)))
    for i, camera in enumerate(cameras):
        click.echo(f'[{i}] {camera}')
    new_roll['camera'] = select_from_list_or_new('camera', cameras)
    films = list(sorted(count_films(rolls)))
    for i, film in enumerate(films):
        click.echo(f'[{i}] {film}')
    new_roll['film'] = select_from_list_or_new('film', films)
    if push_pull := input('Push, pull []: '):
        new_roll['push/pull'] = push_pull
    if expired := input('Expired []: '):
        new_roll['expired'] = expired
    if length := input('Length []: '):
        new_roll['length'] = int(length)
    if lens := input('Lens []: '):
        new_roll['lens'] = lens
    if filter_ := input('Filter []: '):
        new_roll['filter'] = filter_
    if exposed := input('Exposed []: '):
        new_roll['exposed'] = exposed
    if developed := input('Developed []: '):
        new_roll['developed'] = developed
    if developer := input('Developer []: '):
        new_roll['developer'] = developer
    if fixer := input('Fixer []: '):
        new_roll['fixer'] = fixer
    if wet_agent := input('Wet agent []: '):
        new_roll['wet agent'] = wet_agent
    if description := input('Description []: '):
        new_roll['description'] = description
    click.echo(new_roll)


def main() -> None:
    rolls = load_rolls('rolls.txt')
    cli(obj=rolls)


if __name__ == '__main__':
    main()
